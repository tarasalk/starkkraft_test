<?php

namespace app\modules\newsAdmin\models;

use yii\db\ActiveRecord;

class News extends ActiveRecord {

    public function getTheme() {
        return $this->hasOne(NewsTheme::className(), ['id' => 'theme_id']);
    }

    public static function getNewsQuery($limit = null, $offset = null) {
        $query = News::find()
            ->with('theme')
            ->orderBy('date_publication desc');

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }

        return $query;
    }

    public static function getById($id) {
        return News::find()
            ->with('theme')
            ->where(['id' => $id])
            ->one();
    }
}