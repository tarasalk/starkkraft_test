<?php

namespace app\modules\newsAdmin\models;

use yii\db\ActiveRecord;

class NewsTheme extends ActiveRecord {

    public function getNews() {
        return $this->hasMany(News::className(), ['theme_id' => 'id']);
    }

    public static function getThemes() {
        return self::find()->all();
    }
}