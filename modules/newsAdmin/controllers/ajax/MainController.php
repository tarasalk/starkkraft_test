<?php

namespace app\modules\newsAdmin\controllers\ajax;

use Yii;
use app\modules\news\models\News;
use yii\web\Controller;

class MainController extends Controller {

    public function actionAdd() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $data = Yii::$app->request->post();

        $oNews = new News;
        $oNews->theme_id = $data['theme'];
        $oNews->name = $data['name'];
        $oNews->text = $data['text'];
        $oNews->date_publication = $data['date_publication'];

        if ($oNews->save())
            return 'success';

        return 'error';
    }

    public function actionEdit() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $data = Yii::$app->request->post();

        $oNews = News::findOne($data['id']);
        $oNews->theme_id = $data['theme'];
        $oNews->name = $data['name'];
        $oNews->text = $data['text'];
        $oNews->date_publication = $data['date_publication'];

        if ($oNews->save())
            return 'success';

        return 'error';
    }
}
