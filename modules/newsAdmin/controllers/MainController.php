<?php

namespace app\modules\newsAdmin\controllers;

use Yii;
use app\modules\newsAdmin\models\News;
use app\modules\newsAdmin\models\NewsTheme;
use yii\data\Pagination;
use yii\web\Controller;

class MainController extends Controller
{
    const PAGE_LENGTH = 10;
    
    public function actionIndex() {
        $this->view->title = 'Админка новостей';

        $newsCount = News::getNewsQuery()->count();
        $newsPager = new Pagination(['totalCount' => $newsCount, 'pageSize' => self::PAGE_LENGTH]);
        $newsPager->pageSizeParam = false;

        $aNews = News::getNewsQuery($newsPager->limit, $newsPager->offset)->all();
        $aTheme = NewsTheme::find()->all();

        return $this->render('index', compact('aNews', 'aTheme', 'newsPager'));
    }

    public function actionEdit($id) {
        $this->view->title = 'Редактирование новости';
        
        $aTheme = NewsTheme::getThemes();
        $news = News::getById($id);

        return $this->render('edit', compact('news', 'aTheme'));
    }
}
