<script>
    $(function() {
        $("#datePublication").datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    function editNews() {
        $.ajax({
            url: "/newsAdmin/ajax/main/edit",
            type: "POST",
            data: $('#formNewsEdit').serialize(),
            success: function(r) {
                if (r == 'success') {
                    alert('Успешно')
                }
                else {
                    alert(r);
                }

                window.location.reload();
            }
        });
    }
</script>

<form id='formNewsEdit' action="javascript:void(null);" onsubmit="return editNews();" style="width: 300px;">
    <input type="hidden" name="id" value="<?=$news['id']?>">
    <div class="form-group">
        <label for="theme">Тема</label>
        <select class="form-control" id='theme' name="theme">
            <? foreach($aTheme as $theme) {?>
                <option value="<?=$theme['id']?>"><?=$theme['name']?></option>
            <? } ?>
        </select>
    </div>

    <div class="form-group">
        <label for="name">Название</label>
        <input type="text" class="form-control" id="name" name='name' placeholder="Введите название" value="<?=$news['name']?>">
    </div>

    <div class="form-group">
        <label for="text">Текст</label>
        <textarea class="form-control" id="text" name='text' placeholder="Введите текст"><?=$news['text']?></textarea>
    </div>

    <div class="form-group">
        <label for="datePublication">Дата публикации</label>
        <input type="text" id="datePublication" name="date_publication" value="<?=$news['date_publication']?>">
    </div>

    <button type="submit" class="btn btn-success">Сохранить</button>
</form>