<? use yii\widgets\LinkPager; ?>

<script>
    $(function() {
        $("#datePublication").datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    function addNews() {
        $.ajax({
            url: "/newsAdmin/ajax/main/add",
            type: "POST",
            data: $('#formNewsAdd').serialize(),
            success: function(r) {
                if (r == 'success') {
                    alert('Успешно')
                }
                else {
                    alert(r);
                }

                window.location.reload();
            }
        });
    }
</script>

<div>
    <table class="table">
        <thead>
        <tr>
            <th>№</th>
            <th>Тема</th>
            <th>Имя</th>
            <th>Текст</th>
            <th>Дата публикации</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <? foreach($aNews as $news) { ?>
            <tr>
                <td><?=$news['id']?></td>
                <td><?=$news->theme['name']?></td>
                <td><?=$news['name']?></td>
                <td><?=$news['text']?></td>
                <td><?=$news['date_publication']?></td>
                <td><a href="/newsAdmin/main/edit?id=<?=$news['id']?>">ред.</a></td>
            </tr>
        <? } ?>
        </tbody>
    </table>
    <? echo LinkPager::widget([
        'pagination' => $newsPager,
    ]); ?>
</div>

<div>
    <form id='formNewsAdd' action="javascript:void(null);" onsubmit="return addNews();" style="width: 300px;">
        <div class="form-group">
            <label for="theme">Тема</label>
            <select class="form-control" id='theme' name="theme">
                <? foreach($aTheme as $theme) {?>
                    <option value="<?=$theme['id']?>"><?=$theme['name']?></option>
                <? } ?>
            </select>
        </div>

        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="name" name='name' placeholder="Введите название">
        </div>

        <div class="form-group">
            <label for="text">Текст</label>
            <textarea class="form-control" id="text" name='text' placeholder="Введите текст"></textarea>
        </div>

        <div class="form-group">
            <label for="datePublication">Дата публикации</label>
            <input type="text" id="datePublication" name="date_publication" value="<?=date('Y-m-d')?>">
        </div>

        <button type="submit" class="btn btn-success">Добавить</button>
    </form>
</div>