<?php

namespace app\modules\news\models;

use yii\db\ActiveRecord;

class NewsTheme extends ActiveRecord {

    public function getNews() {
        return $this->hasMany(News::className(), ['theme_id' => 'id']);
    }

    public static function getFilterData() {
        $data = self::find()
            ->select(["t.id", "t.name", "COUNT(t.id) count"])
            ->joinWith('news n')
            ->groupBy('n.theme_id')
            ->orderBy('t.id ASC')
            ->alias('t')
            ->asArray()
            ->all();

        return $data;
    }
}