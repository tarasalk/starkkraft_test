<?php

namespace app\modules\news\models;

use app\modules\news\helpers\DateFormat;
use yii\db\ActiveRecord;

class News extends ActiveRecord {

    public function getTheme() {
        return $this->hasOne(NewsTheme::className(), ['id' => 'theme_id']);
    }

    public static function getFilterData() {
        $aNewsFilter = self::find()
            ->select(["YEAR(date_publication) year", "MONTH(date_publication) month", "COUNT(id) count"])
            ->groupBy('YEAR(date_publication), MONTH(date_publication)')
            ->orderBy('YEAR(date_publication) DESC, MONTH(date_publication) DESC')
            ->asArray()
            ->all();

        $aGroupNews = [];
        foreach ($aNewsFilter as $news) {
            $month = DateFormat::getMonthName($news['month']);
            $aGroupNews[$news['year']][$month] = $news['count'];
        }

        return $aGroupNews;
    }

    
    public static function getNewsQuery(array $aFilter = null, $limit = null, $offset = null) {
        $query = News::find()
            ->select(['t.name theme_name', 'n.id', 'n.theme_id', 'n.name', 'LEFT(n.text, 256) news_text', 'n.date_publication'])
            ->joinWith('theme t')
            ->alias('n');

        if ($aFilter['year']) {
            $query->andWhere(['YEAR(date_publication)' => $aFilter['year']]);
        }

        if ($aFilter['month']) {
            $month = DateFormat::getNumMonth($aFilter['month']);
            $query->andWhere(['MONTH(date_publication)' => $month]);
        }

        if ($aFilter['theme_id']) {
            $query->andWhere(['theme_id' => $aFilter['theme_id']]);
        }

        if ($limit) {
            $query->limit($limit);
        }

        if ($offset) {
            $query->offset($offset);
        }

        return $query->asArray();
    }

    
}