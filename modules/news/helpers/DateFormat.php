<?php

namespace app\modules\news\helpers;


class DateFormat {

    public static $monthes = [
        1 => 'Январь' , 2 => 'Февраль' , 3 => 'Март' ,
        4 => 'Апрель' , 5 => 'Май' , 6 => 'Июнь' ,
        7 => 'Июль' , 8 => 'Август' , 9 => 'Сентябрь' ,
        10 => 'Октябрь' , 11 => 'Ноябрь' ,
        12 => 'Декабрь'
    ];

    public static function getMonthName($num) {
        return isset(self::$monthes[$num]) ? self::$monthes[$num] : -1;
    }

    public static function getNumMonth($monthName) {
        $monthes = array_flip(self::$monthes);

        return isset($monthes[$monthName]) ? $monthes[$monthName] : -1;
    }
}