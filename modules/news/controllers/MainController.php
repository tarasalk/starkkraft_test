<?php

namespace app\modules\news\controllers;

use app\modules\news\models\NewsTheme;
use Yii;
use app\modules\news\models\News;
use yii\data\Pagination;
use yii\web\Controller;

class MainController extends Controller
{
    CONST PAGE_LENGTH = 5;

    public function actionIndex() {
        $this->view->title = 'Новости';

        $year = Yii::$app->request->get('year');
        $month = Yii::$app->request->get('month');
        $theme_id = Yii::$app->request->get('theme_id');

        $filterData = ['year' => $year, 'month' => $month, 'theme_id' => $theme_id];

        $newsCount = News::getNewsQuery($filterData)->count();
        $newsPager = new Pagination(['totalCount' => $newsCount, 'pageSize' => self::PAGE_LENGTH]);
        $newsPager->pageSizeParam = false;

        $aNews = News::getNewsQuery($filterData, $newsPager->limit, $newsPager->offset)->all();
        $newsFilter = News::getFilterData();
        $themeFilter = NewsTheme::getFilterData();

        return $this->render('index', compact('newsFilter', 'aNews', 'newsPager', 'themeFilter'));
    }
    
    public function actionItem() {
        $id = Yii::$app->request->get('id', 1);

        $this->view->title = 'Новость '.$id;

        $news = News::findOne($id);

        return $this->render('item', compact('news'));
    }
}
