<? use yii\widgets\LinkPager; ?>

<style>
    ul, li {
        list-style-type: none;
    }

    a {
        cursor: pointer;
    }

    .news {
        border: 1px solid lightgrey;
        padding: 10px;
    }

    .news + .news {
        margin-top: 10px;
    }
</style>


<div class="container">
    <div class="row">
        <div class="col-md-3">
            <ul>
                <li><a href="/news/main">сбросить</a></li>
                <? foreach ($newsFilter as $year => $months) { ?>
                    <li><a href="/news/main?year=<?=$year?>"><?=$year?></a></li>
                    <li>
                        <ul>
                            <? foreach ($months as $monthName => $newsCount) { ?>
                                <li><a href="/news/main?year=<?=$year?>&month=<?=$monthName?>"><?=$monthName?> (<?=$newsCount?>)</a></li>
                            <? } ?>
                        </ul>
                    </li>
                <? } ?>
            </ul>
            <ul>
                <? foreach ($themeFilter as $theme) { ?>
                    <li><a href="/news/main?theme_id=<?=$theme['id']?>"><?=$theme['name']?> (<?=$theme['count']?>)</a></li>
                <? } ?>
            </ul>
        </div>
        <div class="col-md-7">
            <? foreach ($aNews as $news) { ?>
                <div class="news">
                    <div>Название: <?=$news['name']?></div>
                    <div>
                        Дата публикации: <?=$news['date_publication']?>.
                        Тема: <?=$news['theme']['name']?>
                    </div>
                    <div style="overflow: hidden;">Краткий текст: <?=$news['news_text']?>...</div>
                    <div style="overflow: hidden">
                        <a href="/news/main/item?id=<?=$news['id']?>" style="float: right;  ">
                            читать далее
                        </a>
                    </div>
                </div>
            <? } ?>
            <? echo LinkPager::widget([
                'pagination' => $newsPager,
            ]); ?>
        </div>
    </div>
</div>

